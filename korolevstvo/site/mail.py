import time
import datetime

import requests
import re

from ..main import postRequest, getRequest, LoginData
from ..data import Money

# === === Parcel === === #

class Parcel():
	""" """
	def __init__(self, messageId: int, item: str, cost: Money, resiveTime: datetime.timedelta, isResived: bool):
		self.__messageId = messageId
		self.__item = item
		self.__cost = cost
		self.__isResived = isResived
		self.__resiveTime = resiveTime

	@property
	def message(self) -> int:
		""" """
		return self.__messageId

	@property
	def item(self) -> str:
		""" """
		return self.__item

	@property
	def cost(self) -> Money:
		""" """
		return self.__cost

	@property
	def resiveTime(self):
		""" """
		return self.__resiveTime

	@property
	def isResived(self) -> bool:
		""" """
		return self.__isResived

	@isResived.setter
	def setResived(self, value: bool) -> None:
		""" """
		self.__isResived = value

	def __eq__(self, other) -> bool:
		""" """
		return self.__messageId == other.__messageId

# === === MailMessage === === #

class MailMessage():
	""" """
	def __init__(self, sender: str, id_: int, **kwargs):
		self.__sender = sender
		self.__id = id_
		self.__date = kwargs["date"] if "date" in kwargs else datetime.datetime(1970, 1, 1)
		self.__text = kwargs["text"] if "text" in kwargs else str()
		self.__header = kwargs["header"] if "header" in kwargs else str()
		self.__isRead = kwargs["isRead"] if "isRead" in kwargs else False
		self.__parcel = kwargs["parcel"] if "parcel" in kwargs else None

	@property
	def sender(self) -> str:
		""" """
		return self.__sender

	@property
	def id_(self) -> int:
		""" """
		return self.__id_

	@property
	def date(self) -> datetime.datetime:
		""" """
		return self.__date

	@property
	def header(self) -> str:
		""" """
		return self.__header

	@property
	def isRead(self) -> bool:
		""" """
		return self.__isRead

	@isRead.setter
	def setRead(self, value: bool) -> None:
		""" """
		self.__isRead = value

	@property
	def parcel(self):
		""" """
		return self.__parcel

# === === Methods === === #

def sendMessage(loginData: LoginData, recipient: str, header: str, message: str) -> requests.Response:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/send/"

	data = {
		"username": recipient,
		"in_reply": 0,
		"is_ajax": 1,
		"mail_subj": header,
		"mail_message": message
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/compose/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
	}

	return postRequest(url, loginData.cookies, headers, data)

def markMessageRead(loginData: LoginData, messageId: int) -> requests.Response:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/mark_read/"

	data = {
		"message_id[]": str(messageId)
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def markMessagesListRead(loginData: LoginData, messagesList: list) -> requests.Response:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/mark_read/"

	data = {
		"message_id[]": [str(i) for i in messagesList]
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def getFreeParcel(loginData: LoginData, messageId: int) -> requests.Response:
	""" """

	t = time.time()
	url = f"https://{loginData.nickname}.kor.ru/mail/admin_package/add_to_bag/?id={messageId}&_={t}"

	data = {
		"id": str(messageId),
		"_": str(t)
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return getRequest(url, loginData.cookies, headers, data)

def getMoneyFromParcel(loginData: LoginData, messageId: int) -> requests.Response:
	""" """

	t = time.time()
	url = f"https://{loginData.nickname}.kor.ru/mail/money/add_to_bag/?id={messageId}&_={t}"

	data = {
		"id": str(messageId),
		'_': str(t)
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def buyParcel(loginData: LoginData, messageId) -> requests.Response:
	""" """

	if isinstance(messageId, Parcel):
		messageId = messageId.message

	t = time.time()
	url = f"https://{loginData.nickname}.kor.ru/mail/package/add_to_bag/?id={messageId}&_={t}"

	data = dict()
	# or data = {"id": str(messageId), '_': str(t)}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def denyParcel(loginData: LoginData, messageId):
	""" """

	if isinstance(messageId, Parcel):
		messageId = messageId.message

	t = time.time()
	url = f"https://{loginData.nickname}.kor.ru/mail/package/deny/?id={messageId}&_={t}"

	data = dict()
	# or data = {"id": str(messageId), '_': str(t)}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def deleteMessage(loginData: LoginData, messageId: int) -> requests.Response:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/delete/"

	data = {
		"message_id[]": str(messageId)
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def deleteMessagesList(loginData: LoginData, messagesList: list) -> requests.Response:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/delete/"

	data = {
		"message_id[]": [str(i) for i in messagesList]
	}

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/mail/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Accept": "application/xml, text/xml, */*; q=0.01"
	}

	return postRequest(url, loginData.cookies, headers, data)

def _messageTextAndParcelProcess(message: str) -> MailMessage:
	""" """

	# Получение ID
	id_ = re.search(r"(?<=li id=\"u_message_)\d+", message)[0]
	id_ = int(id_)

	# Получение никнейма отправителя
	sender = re.search(r"(?<=<span class=\"text_12\">).+?(?=</span>)", message)
	if (sender != None):
		sender = sender[0]

	# Получение даты. Временно не работает! to do
	date = re.search(r"(?<=<div class=\"floatleft\"><span class=\"text12\">).+", message)
	if (date != None):
		date = date[0]
		date = date.strip()

	# Получение заголовка
	title = re.search(r"(?<=<strong>).+?(?=</strong>)", message)
	if (title != None):
		title = title[0]

	# Получение текста. htm2text нужен. to do
	text = re.search(r"(?s)(?<=</div><span class=\"text12\">).+?(?=</span>)", message)
	if (text != None):
		text = text[0]

	parcel = None
	# Проверка наличия посылки и определение ее типа
	# if re.search(r"<div class=\"item \"><div class=\"title\">", message) is not None:
	# 	# Предмет, который не выкуплен вами
	# 	if re.search(r"<a href=\"#\" id=\"u_buy_package", message) is not None:
	# 		pass

	# 	# Предмет, который выкуплен другим игроком на почте / аукционе, но деньги не взяты
	# 	elif re.search(r"<a href=\"#\" class=\"u_money_to_bag u_button_loader\" id=\"u_money_to_bag_", message) is not None:
	# 		pass

	# 	# Бесплатный предмет, который вами не взят
	# 	elif re.search(r"<div class=\"buttons\" id=\"u_package_buttons_", message) is not None:
	# 		pass

	return MailMessage(sender, id_, date=date, title=title, text=text, parcel=parcel)

def getMessagesPage(loginData: LoginData, pageNumber: int) -> list:
	""" """

	url = f"https://{loginData.nickname}.kor.ru/mail/p/{pageNumber}"

	if pageNumber < 0:
		return list()

	headers = {
		"Host": f"{loginData.nickname}.kor.ru",
		"Referer": f"https://{loginData.nickname}.kor.ru/",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
	}

	processText = getRequest(url, loginData.cookies, headers).text # HTML текст страницы
	processText = re.findall(r"(?s)<li id=.+?</li>", processText) # Список сообщений (html)

	messages = list()

	for message in processText:
		messages.append(_messageTextAndParcelProcess(message))

	return messages

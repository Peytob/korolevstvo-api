class LoginException(Exception):
	"""Ошибка входа в игру: введены неверные данные или данные для входа устарели."""
	def __init__(self, text: str):
		self.txt = text

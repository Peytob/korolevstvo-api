import requests
import re
import copy

from .data import LoginData
from .exceptions import LoginException
from .configuration import standartHeaders

def login(nickname: str, password: str) -> LoginData:
	"""Логин в игре по заданным данным.
	Параметры:
		nickname [str] - Имя персонажа или электронная почта.
		password [str] - Пароль.
	Возвращаемое значение:
		[LoginData] Данные авторизации: имя персонажа, пароль и cookie.
	Исключения:
		requests.exceptions.RequestException.
		LoginException - В случае, если введены неверные данные для входа."""
	
	url = "https://www.kor.ru/login/"

	data = {
		"login": nickname,
		"password": password,
		"return_url": ''
	}

	serverResponse = requests.post(url, data, standartHeaders)

	if serverResponse.url == url: # Возвращение https://kor.ru/login/ обозначает неверные данные для входа
		raise LoginException("Bad login.")

	if nickname.find('@') != -1:
		nickname = re.search(R"(?<=//).+?(?=\.)", serverResponse.url)
	
	interfaceNickname = re.search(R"""(?<="color3 text11">).+?(?=<)""", serverResponse.text)

	return LoginData(nickname, interfaceNickname, serverResponse.cookies)

def postRequest(url: str, cookies, headers: dict, data: dict) -> requests.Response:
	""" """

	requestHeaders = copy.deepcopy(standartHeaders)
	requestHeaders.update(headers)

	return requests.request(method="POST", url=url, headers=requestHeaders, data=data, cookies=cookies)

def getRequest(url: str, cookies, headers: dict, data = None) -> requests.Response:
	""" """

	requestHeaders = copy.deepcopy(standartHeaders)
	requestHeaders.update(headers)

	return requests.request(method="GET", url=url, headers=requestHeaders, data=data, cookies=cookies)

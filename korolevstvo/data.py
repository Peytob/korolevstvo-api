import copy

import requests

# === === LoginData === === #

class LoginData():
	""" """
	def __init__(self, nickname: str, UINickname: str, cookies: requests.cookies.RequestsCookieJar):
		self.__nickname = nickname
		self.__UINickname = UINickname
		self.__cookies = cookies

	@property
	def nickname(self) -> str:
		""" """
		return self.__nickname

	@property
	def UINickname(self) -> str:
		""" """
		return self.__UINickname

	@property
	def cookies(self) -> requests.cookies.RequestsCookieJar:
		""" """
		return self.__cookies

# === === Money === === #

class Money():
	""" """

	def __init__(self, gold: int, silver: int, copper: int):
		self.__gold = 0
		self.__silver = 0
		self.__copper = 0

		amount = copper + 100*silver + 10000*gold
		negative = amount < 0
		amount = abs(amount)

		self.__gold = int(amount / 10000)
		amount %= 10000
		self.__silver = int(amount / 100)
		self.__copper = amount % 100

		if negative:
			self.__gold = -self.__gold
			self.__silver = -self.__silver
			self.__copper = -self.__copper

	@property
	def gold(self) -> int:
		""" """
		return self.__gold

	@property
	def silver(self) -> int:
		""" """
		return self.__silver

	@property
	def copper(self) -> int:
		""" """
		return self.__copper

	@property
	def amount(self) -> int:
		""" """
		return self.__copper + 100*self.__silver + 10000*self.__gold

	# Operators
	def __lt__(self, other):
		""" """
		return self.amount < other.amount

	def __le__(self, other):
		""" """
		return self.amount <= other.amount

	def __eq__(self, other):
		""" """
		return self.amount == other.amount

	def __ne__(self, other):
		""" """
		return self.amount != other.amount

	def __gt__(self, other):
		""" """
		return self.amount > other.amount

	def __ge__(self, other):
		""" """
		return self.amount >= other.amount

	def __str__(self):
		""" """
		return f"{self.__gold} gold, {self.__silver} silver, {self.__copper} copper"

	def __add__(self, other):
		""" """
		rgold = self.__gold + other.__gold
		rsilver = self.__silver + other.__silver
		rcopper = self.__copper + other.__copper

		return Money(rgold, rsilver, rcopper)

	def __sub__(self, other):
		""" """
		return self + (-other)

	def __neg__(self):
		""" """
		copper = -self.__copper
		silver = -self.__silver
		gold = -self.__gold

		return Money(gold, silver, copper)

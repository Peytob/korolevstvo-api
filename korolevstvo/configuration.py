import requests

standartHeaders = requests.cookies.cookiejar_from_dict({
	"Accept":"application/xml, text/xml, */*; q=0.01",
	"Accept-Encoding":"gzip, deflate",
	"Accept-Language":"ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
	"Cache-Control":"no-cache",
	"Connection":"keep-alive",
	"Content-Type":"__CONTENT-TYPE__", # Description of data sent in the request
	"Host":"__HOST__", # nickname.kor.ru/...
	"Pragma":"no-cache",
	"Referer":"__REFERER__", # https://nickname.kor.ru/ ...
	"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0",
	'X-Requested-With': "XMLHttpRequest"
})
